'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class shirt extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      shirt.belongsTo(models.user, {
        foreignKey: 'user_id'
      });
    };
  }
  shirt.init({
    user_id: DataTypes.INTEGER,
    merk: DataTypes.STRING,
    size: DataTypes.STRING,
    price: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'shirt',
  });
  return shirt;
};