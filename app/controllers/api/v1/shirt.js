const { shirt,user } = require("../../../models");
const shirtService = require("../../../services/shirt");

module.exports = {
  list(req, res) {
      shirtService.getAllShirt({
        include:{
          model : user,
        }
      })
      .then((shirt) => {
        res.status(200).json({
          status: "OK",
          data: {
            shirt,
          },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
      

  create(req, res) {
    const { user_id, merk, size, price} = req.body;
    shirtService.createShirt( user_id, merk, size, price )
      .then((shirt) => {
        res.status(201).json({
          status: "OK",
          data: shirt,
        });
      })
      .catch((err) => {
        res.status(401).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  update(req, res) {
    const shirt = req.shirt;
    shirtService.updateShirt( shirt, req.body )
      .then(() => {
        res.status(200).json({
          status: "OK",
          data: shirt,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    const shirt = req.shirt;

    res.status(200).json({
      status: "OK",
      data: shirt,
    });
  },

  destroy(req, res) {
    shirtService
    .deleteShirt(req.shirt)
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  setShirt(req, res, next) {
    shirtService.findKey(req.params.id)
      .then((shirt) => {
        if (!shirt) {
          res.status(404).json({
            status: "FAIL",
            message: "Post not found!",
          });

          return;
        }

        req.shirt = shirt;
        next()
      })
      .catch((err) => {
        res.status(404).json({
          status: "FAIL",
          message: "Post not found!",
        });
      });
  },
};
