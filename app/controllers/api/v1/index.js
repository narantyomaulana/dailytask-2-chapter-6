/**
 * @file contains entry point of controllers api v1 module
 * @author Fikri Rahmat Nurhidayat
 */

// const post = require("./post");
const user = require("./user");
const shirt = require("./shirt");

module.exports = {
  user,
  shirt
};
