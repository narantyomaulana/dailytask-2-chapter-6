const { shirt } = require("../models");

module.exports = {
    getAllShirt(a) {
        const shirts = shirt.findAll(a)
        return shirts
    },
    createShirt(user_id, merk, size, price){
        const shirts = shirt.create({
            user_id, merk, size, price
        });
        return shirts;
    },

    updateShirt(shirt, shirtUpdate){
        return shirt.update(shirtUpdate)
    },
    deleteShirt(shirt){
        return shirt.destroy()
    },
    findKey(findIdShirt){
        return shirt.findByPk(findIdShirt)
    }
}
